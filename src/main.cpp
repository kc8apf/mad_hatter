#include <Adafruit_GFX.h>
#include <Adafruit_SharpMem.h>

#include "hat_00.xbm"
#include "hat_01.xbm"
#include "hat_02.xbm"
#include "hat_03.xbm"
#include "hat_04.xbm"
#include "hat_05.xbm"
#include "hat_06.xbm"
#include "hat_07.xbm"
#include "hat_08.xbm"
#include "hat_09.xbm"

/* Changelog:
 * v1.0: Initial build
 * v1.1:
 *   - Move images to PROGMEM to cut down RAM usage
 * v1.2:
 *   - Add splash screen showing total # of hats
 *   - Loop through images in random sequence instead of picking a single random
 *     image at boot.
 * v1.3:
 *   - Add hat number in bottom-left corner so they can be ID'd for removal.
 *   - Center text on splash screen.
 * v1.4:
 *   - When pciking the next random hat, prevent repeats.
 */
#define VERSION "1.4"

// any pins can be used
#define SCK 10
#define MOSI 11
#define SS 13

#define BLACK 0
#define WHITE 1

const uint8_t* const hats[] = {
  hat_00_bits,
  hat_01_bits,
  hat_02_bits,
  hat_03_bits,
  hat_04_bits,
  hat_05_bits,
  hat_06_bits,
  hat_07_bits,
//  hat_08_bits,  // Hard to recognize
  hat_09_bits
};
#define NUM_HATS (sizeof(hats)/sizeof(uint8_t*))

Adafruit_SharpMem display(SCK, MOSI, SS);

void setup(void) 
{
  // start & clear the display
  display.begin();
  display.clearDisplay();

  // Version info
  display.setTextSize(1);
  display.setTextColor(BLACK);

  display.setCursor(20,0);
  display.println("Mad Hatter");

  display.setCursor(20,10);
  display.setTextColor(WHITE, BLACK); // 'inverted' text
  display.println("by kc8apf");

  display.setCursor(15,30);
  display.setTextColor(BLACK);
  display.println("Version: " VERSION);

  display.setCursor(3,60);
  display.print("Hats loaded: "); display.println(NUM_HATS);
  // Screen must be refreshed at least once per second.  Do so for 2 seconds.
  display.refresh();
  delay(500);
  display.refresh();
  delay(500);
  display.refresh();
  delay(500);
  display.refresh();
  delay(500);


  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0)); 
}

void loop(void) 
{
  // Select an initial hat to display at random.
  static int selected_hat = random(NUM_HATS);
  
  display.clearDisplay();
  display.drawXBitmap(0, 0, hats[selected_hat], 96, 96, BLACK);
  display.setCursor(1, 86);
  display.println(selected_hat);

  // Screen must be refreshed at least once per second
  display.refresh();
  delay(500);

  // Select a new hat.  Since one hat is already on display, only NUM_HATS-1 are
  // available to choose from.  Map [0,NUM_HATS-1) onto the indices of all
  // currently undisplayed hats by shifting the range to an offset from the
  // current hat (with a minimum offset of 1 hat) and using modulus to wrap the
  // range back to the beginning of the array as needed.
  selected_hat = (selected_hat + 1 + random(NUM_HATS-1)) % NUM_HATS;

  display.refresh();
  delay(500);
  display.refresh();
  delay(500);
  display.refresh();
  delay(500);
}
